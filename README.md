# mirror_stats

Collects and calculates stats from nginx

## Setup

This operates on `kvaser` and container `logserver` on vm2.

### Kvaser

Nginx on `kvaser` is setup to log all 200 and 300 request to an rsyslog server on `logserver` 10.0.0.102.

It looks a bit like the following but split up in two files: `nginx.conf` and `dotsrc-shared.conf`
```
 log_format testing '$request_length $uri';
 map $status $iftotest {
    ~^[23]   1;
    default 0;
}                                
access_log syslog:server=10.0.0.102 testing;
```

### Logserver

On the logserver rsyslog is configured to pass all things on udp 514 to the counter program.

```
input(type="imudp" address="10.0.0.102" port="514" ruleset="mirror")
$template mirrortmpl,"%msg%\n"
ruleset(name="mirror") {
        action(type="omprog" binary="/usr/bin/counter -d 2 -i -o /var/log/countstat -c" output="/var/log/counter.log" template="mirrortmpl")
}
```

Is happens in `/etc/rsyslog.conf` and `/etc/rsyslog.d/20-mirror.conf`.

`/usr/bin/counter` is compiled as follows:

```
cd /root/mirror_stats/counter
go build
install counter /usr/bin/counter
```

Stats are therefore in `/var/log/countstat`.

The analyzer program runs from its own crontab every night at 00:15.

```
15 0 * * * python3 /home/mirror_analyze/analyze/analyze.py --span all --output /srv/mirror_analyze/all.html --sortby s --offset 0 /var/log/countstat /home/mirror_analyze/analyze/webtmpl.html
15 0 * * * python3 /home/mirror_analyze/analyze/analyze.py --span day --output /srv/mirror_analyze/day.html --sortby s --offset -1 /var/log/countstat /home/mirror_analyze/analyze/webtmpl.html
```

So webpages are saved to `/srv/mirror_analyze/`.

