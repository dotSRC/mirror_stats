// This file is part of mirror_stats.
// 
// mirror_stats is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// mirror_stats is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with mirror_stats.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
    "strings"
    "errors"
    "strconv"
)

type Request struct {
    Path []string
    Size uint64
}

var (
    ErrNotEnoughFields = errors.New("Not enough fields")
)

// depth=-1 for infinite
func splitPath(path string, depth int) []string {
    var res []string

    var start int = 0
    appendto := func(to int) {
        if to - start > 1 {
            res = append(res, path[start:to])

        }
    }
    for i, c := range path {
        if c == '/' {
            appendto(i)
            if depth > 0 && len(res) >= depth {
                return res
            }
            start = i+1
            continue
        }
    }

    appendto(len(path))

    return res
}

func ParseReq(msg string, depth int) (*Request, error) {
    msg = strings.TrimPrefix(msg, " ")
    index := strings.Index(msg, " ")
    if index == -1 {
        return nil, ErrNotEnoughFields
    }

    size, err := strconv.ParseUint(msg[:index], 10, 64)
    if err != nil {
        return nil, err
    }

    return &Request{
        Path: splitPath(msg[index+1:], depth),
        Size: size,
    }, nil
}
