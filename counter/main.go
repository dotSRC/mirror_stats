// This file is part of mirror_stats.
// 
// mirror_stats is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// mirror_stats is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with mirror_stats.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
    "log"
    "os/signal"
    "syscall"
    "os"
    "flag"
    "bufio"
)

func onINT(f func()) {
    c := make(chan os.Signal)
    signal.Notify(c, os.Interrupt, syscall.SIGTERM)
    go func() {
        <-c
        f()
        os.Exit(0)
    }()
}

func main() {
    log.Println("Starting")

    dest := flag.String("o", "out", "folder to write result files")
    incomplete := flag.Bool("i", false, "save stats for partial hours")
    depth := flag.Int("d", -1, "level of paths to save")
    compress := flag.Bool("c", false, "compress output with gzip")

    flag.Parse()

    counter := NewCounter(*incomplete)
    printer := NewPrinter(counter, *dest, *compress)

    onINT(func() {
        printer.Stop()
    })

    scanner := bufio.NewScanner(os.Stdin)

    for scanner.Scan() {
        req, err := ParseReq(scanner.Text(), int(*depth))
        if err != nil {
            log.Printf("Err parsing %q: %s", scanner.Text(), err)
            continue
        }

        counter.Handle(req)
    }

    printer.Stop()
}
