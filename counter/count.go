// This file is part of mirror_stats.
// 
// mirror_stats is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// mirror_stats is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with mirror_stats.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
    "sync"
    "encoding/json"
    "io"
    "compress/gzip"
    "log"
    "time"
    "path"
    "os"
)

type Path struct {
    Hits uint32 `json:"h"`
    Size uint64 `json:"s"`
    Paths map[string]*Path `json:"p,omitempty"`
}

type Stats struct {
    Root *Path `json:"r"`
    Start time.Time `json:"-"`
}

type Counter struct {
    lock    sync.Mutex
    current *Stats
    Incomplete bool
}

type Printer struct {
    stop    chan struct{}
    finish    chan struct{}

    file *os.File

    // Config
    path string
    compress bool
}

type outEntry struct {
    Stats
    Start string `json:"s"`
    End string `json:"e"`
}

func NewPath() *Path {
    return &Path{
        Paths: map[string]*Path{},
    }
}

func (p *Path) Add(path []string, size uint64) {
    p.Hits++
    p.Size += size

    if len(path) == 0 {
        return
    }

    sub, ok := p.Paths[path[0]]
    if !ok {
        sub = NewPath()
        p.Paths[path[0]] = sub
    }
    sub.Add(path[1:], size)
}

func NewStats() *Stats {
    return &Stats{
        Root: NewPath(),
        Start: time.Now(),
    }
}

func (s *Stats) Add(path []string, size uint64) {
    s.Root.Add(path, size)
}

func NewCounter(incomplete bool) *Counter {
    res := &Counter {
        current: nil,
        Incomplete: incomplete,
    }

    if incomplete {
        res.current = NewStats()
    }

    return res
}

func (c *Counter) Handle(req *Request) {
    if c.current != nil {
        c.current.Add(req.Path, req.Size)
    }
}

func (c *Counter) FetchStats() *Stats {
    c.lock.Lock()
    old := c.current
    c.current = NewStats()
    c.lock.Unlock()

    return old
}

func NewPrinter(c *Counter, path string, compress bool) *Printer {
    p := Printer{
        stop: make(chan struct{}),
        finish: make(chan struct{}),
        path: path,
        compress: compress,
    }

    // This function sets up the output writer!!
    timer := time.NewTimer(timeTilNextPeriod(time.Now()))

    go func () {
        for {
            dostop := false
            select {
            case <- timer.C:
            case <- p.stop:
                dostop = true
            }

            if dostop && !c.Incomplete {
                break
            }

            stats := c.FetchStats()
            if stats != nil {
                err := p.printEntry(stats)
                if err != nil {
                    log.Printf("ERR: %s", err.Error())
                }
            }

            if dostop {
                break
            }

            timer.Reset(timeTilNextPeriod(time.Now()))
        }
        p.finish <- struct{}{}
    }()

    return &p
}

func nextPeriod(now time.Time) time.Time {
    // Kind of hacky
    newhour := now.Hour()+1
    if now.Hour() == 23 {
        now = now.Add(2 * time.Hour)
        newhour = 0
    }

    return time.Date(
        now.Year(),
        now.Month(),
        now.Day(), newhour, 0, 0, 0, now.Location(),
    )

}

func timeTilNextPeriod(now time.Time) time.Duration {
    target := nextPeriod(now)
    return target.Sub(now)
}

// Maybe it's not that smart to open and close. But if it's only every hour
// it's probably fine
func (p *Printer) printEntry(s *Stats) error {
    e := outEntry{
        Stats: *s,
        Start: s.Start.Format("15:04:05"),
        End: time.Now().Format("15:04:05"),
    }
    fname := path.Join(p.path, s.Start.Format("2006-01-02.jsonl"))
    if p.compress {
        fname += ".gz"
    }

    log.Printf("Writing to %q", fname)

    f, err := os.OpenFile(fname, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
    if err != nil {
        return err
    }
    defer f.Close()

    var w io.WriteCloser = f
    if p.compress {
        w = gzip.NewWriter(w)
        defer w.Close()
    }

    err = json.NewEncoder(w).Encode(e)
    if err != nil {
        return err
    }

    return nil
}

func (p *Printer) Stop() {
    p.stop <- struct{}{}

    <-p.finish
}
