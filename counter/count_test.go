// This file is part of mirror_stats.
// 
// mirror_stats is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// mirror_stats is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with mirror_stats.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
    "testing"
    "time"
)

const timeformat = "2006-01-02T15:04"

var nextPeriodCases = []struct{
    Before string
    After string
} {
    {"2020-09-24T15:10", "2020-09-24T16:00"},
    {"2020-09-24T15:00", "2020-09-24T16:00"},
    {"2020-09-24T00:10", "2020-09-24T01:00"},
    {"2020-09-24T15:59", "2020-09-24T16:00"},
    {"2020-09-24T11:15", "2020-09-24T12:00"},
    {"2020-09-24T23:15", "2020-09-25T00:00"},
    {"2020-09-24T23:59", "2020-09-25T00:00"},
    {"2020-09-24T23:00", "2020-09-25T00:00"},
}

func TestNextPeriod(t *testing.T) {
    parsetime := func (str string) time.Time {
        ti, err := time.Parse(timeformat, str)
        if err != nil {
            t.Errorf("parsing test cases: %s", err.Error())
        }

        return ti
    }
    for _, c := range nextPeriodCases {
        before := parsetime(c.Before)
        after := parsetime(c.After)

        res := nextPeriod(before)
        if after != res {
            t.Errorf("nextPeriod(%v) = %v, want %v", before, res, after)
        }
    }
}
