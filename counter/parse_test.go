// This file is part of mirror_stats.
// 
// mirror_stats is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// mirror_stats is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with mirror_stats.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
    "testing"
    "reflect"
)

var cutPathCases = []struct{
    Before string
    After []string
} {
    {"/hej/med/dig", []string{"hej", "med"}},
    {"/hej//med/dig", []string{"hej", "med"}},
    {"/hej///med/dig", []string{"hej", "med"}},
    {"///hej///med/dig", []string{"hej", "med"}},
    {"//hej//med/dig", []string{"hej", "med"}},
    {"//hej//", []string{"hej"}},
    {"//hej", []string{"hej"}},
    {"/hej", []string{"hej"}},
    {"hej", []string{"hej"}},
    {"hej/", []string{"hej"}},
    {"hej/med", []string{"hej", "med"}},
    {"////", nil},
    {"", nil},
    {"/", nil},
    {"/hej/med/", []string{"hej", "med"}},
}

func TestCutPath(t *testing.T) {
    for _, c := range cutPathCases {
        res := cutPath(c.Before, 2)
        if !reflect.DeepEqual(c.After, res) {
            t.Errorf("nextPeriod(%v) = %v, want %v", c.Before, res, c.After)
        }
    }
}
