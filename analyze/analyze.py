#!/usr/bin/env python3
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import argparse
import gzip
import json
import glob
import io
import os
import sys
import os.path as path
from datetime import datetime as dt
from datetime import timedelta
from jinja2 import Environment, BaseLoader, select_autoescape, TemplateNotFound
import re
import numpy as np
import matplotlib.pyplot as plt

timeoptions = ["day", "week", "month", "all"]
filenameregex = re.compile("(\d{4}-\d\d-\d\d).*")
def intime(span, now, filename, offset):
    m = filenameregex.match(filename)
    if m == None:
        return None
    t = dt.strptime(m[1], "%Y-%m-%d")

    if span not in ["day", "all"]:
        sys.exit(f"span {span} not implemented")

    if span == "all":
        return t
    elif (now.date()+timedelta(days=offset)) == t.date():
        return t
    return None

def loadfile(fname):
    dat = []
    openfunc = gzip.open if fname.endswith(".gz") else open
    with openfunc(fname, "rb") as f:
        for line in f:
            dat.append(json.loads(line.decode("utf-8")))

    return dat

def analyze_tree(tree, depth):
    if tree["h"] == 0:
        return
    tree["avg"] = tree["s"] / tree["h"]
    if depth == 0:
        return
    for sub, val in tree["p"].items():
        analyze_tree(val, depth-1)
    return tree

def analyze(data, depth):
    for entry in data:
        analyze_tree(entry["r"], depth)

def merge_tree(dest, src, depth):
    dest["s"] += src["s"]
    dest["h"] += src["h"]

    if depth == 0:
        return
    for path in src["p"].keys():
        if path not in dest["p"].keys():
            dest["p"][path] = {"s": 0, "h": 0, "p": {}}
        merge_tree(dest["p"][path], src["p"][path], depth-1)

def sum_entries(data, depth):
    res = {"s": 0, "h": 0, "p": {}}
    for entry in data:
        merge_tree(res, entry["r"], depth)
    return res

def sort_sum(data, sortby):
    l = len(data.keys()) * [None]
    index = 0
    for k, v in data.items():
        entry = {"path": k}
        entry.update(v)
        l[index] = entry
        index += 1

    l = sorted(l, reverse=True, key=lambda e: e[sortby])
    return l

# Only works for daily, data should be the original with timestamp
def create_graph(data, toplist, n=3, metric="h"):
    # See... hardcoded to a 24 hour day.
    x = np.arange(24)
    ys = {}
    for top in toplist[:n]:
        ys[top["path"]] = np.zeros(24, dtype=np.long)

    for entry in data:
        t = dt.strptime(entry["s"], "%H:%M:%S")
        if t == None:
            continue
        for path, value in entry["r"]["p"].items():
            if path in ys:
                ys[path][t.hour] = value[metric]

    fig, ax = plt.subplots()
    for name, y in ys.items():
        print(name, y)
        ax.plot(x, y, label=name)

    ax.set(xlabel="time [h]", ylabel="hits")
    ax.grid()
    ax.legend()
    ax.set_yscale("log")

    img = io.StringIO()
    fig.savefig(img, format="svg")
    img.seek(0)
    print(img.getvalue())

def bytesfmt(b):
    units = ["", "Ki", "Mi", "Gi", "Ti", "Pi"]
    for u in units:
        if abs(b) < 1024:
            return "%0.2f %sB" % (b, u)
        b /= 1024
    return "%0.2f %sB" % (b, "Ei")

parser = argparse.ArgumentParser()
parser.add_argument("logdir", help="dir where stat files are located")
parser.add_argument("template", help="template to sender from")
parser.add_argument("--span", "-s", help="period to load data from", default="all", choices=timeoptions)
parser.add_argument("--output", "-o", help="where to output rendered template, default stdout")
parser.add_argument("--depth", "-d", default=1, help="how deep to analyze tree")
parser.add_argument("--sortby", default="s", help="what to sort values by, Hits, Size and AVeraGe", choices=["h", "s", "avg"])
parser.add_argument("--offset", type=int, default=-1, help="offset the span, to take yesterday instead of today. Does not do anything when span=all")

args = parser.parse_args()

fileglob = "*.jsonl*"

statfiles = glob.glob(path.join(args.logdir, fileglob))
statfiles.sort(reverse=True)

entries = []

for fname in statfiles:
    t = intime(args.span, dt.now(), path.basename(fname), args.offset)
    if not t:
        continue

    entries.extend(loadfile(fname))

# Setup jinja stuff COPIED from mirror_ood
# Can't find a jinja2 loader which just loads a single file from a absolute
# path. Taken from https://jinja.palletsprojects.com/en/2.11.x/api/#jinja2.BaseLoader
class FileLoader(BaseLoader):
    def get_source(self, environment, template):
        if not os.path.exists(template):
            raise TemplateNotFound(template)
        mtime = os.path.getmtime(template)
        with open(template, "r") as f:
            source = f.read()
        return source, template, lambda: mtime == getmtime(template)

# Why is this so complicated
env = Environment(
        loader=FileLoader(),
        autoescape=select_autoescape(['html', 'xml'])
        )
env.globals.update(bytesfmt=bytesfmt)

page = {
        "sum": analyze_tree(sum_entries(entries, args.depth), args.depth),
        "now": dt.now(),
        }
page["sorted"] = sort_sum(page["sum"]["p"], args.sortby) if page["sum"] != None else []

t = env.get_template(args.template)
render = t.render(page)
if args.output:
    with open(args.output, "w") as f:
        f.write(render)
else:
    sys.stdout.write(render)
