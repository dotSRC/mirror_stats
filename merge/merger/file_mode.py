"""
    This file is part of mirror_stats.

    mirror_stats is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    mirror_stats is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with mirror_stats.  If not, see <https://www.gnu.org/licenses/>.
"""

from .merge import TimeEntry
import json
import gzip

import os.path as path
def file_entries(filename, dofiletime=False):
    if dofiletime:
        filetime = path.basename(filename).split(".")[0]
    else:
        filetime = None

    entries = []

    openfunc = gzip.open if filename.endswith(".gz") else open
    with openfunc(filename, "r") as f:
        for line in f:
            entries.append( TimeEntry(json.loads(line), filetime) )

    return entries

def file_cmd_f(args):

    entries = file_entries(args.infile, True)

    summed = TimeEntry.merge_list(entries)

    if args.outfile != None:
        with open(args.outfile, "w") as f:
            json.dump(summed.to_dict(), f)
    else:
        print(json.dumps(summed.to_dict()))

