"""
    This file is part of mirror_stats.

    mirror_stats is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    mirror_stats is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with mirror_stats.  If not, see <https://www.gnu.org/licenses/>.
"""

from .file_mode import file_entries
from .merge import TimeEntry
import os.path as path
import os
import re
import json
import signal

# Hmm should probably prefix with something
raw_regex = re.compile("\d{4}-\d\d-\d\d.jsonl")

class SingleFileLoadSaver:
    def __init__(self, folder):
        # Maps a file category to a loaded file TimeEntry
        self.loaded = {}
        # Same a loaded but with corrosponding pathnames
        self.loadedpaths = {}

        self.unsaved = set()

        self.folder = folder

        # Contains a dictionary of files which are still saved as temp files
        # The key is the real name while the value is the temp name
        self.commits = {}

    def save(self, cat):
        fname = self.loadedpaths[cat]

        # Find the temp name
        tmpname = fname + ".temp"
        fpath = path.join(self.folder, tmpname)
        print(f"Saving {fpath}")

        with open(fpath, "w") as f:
            json.dump(self.loaded[cat].to_dict(), f)

        self.unsaved.remove(cat)
        self.commits[fname] = tmpname

    # Will try to load from fname, if not exists put in otherwise
    # Will return true if the file was loaded
    def load(self, cat, fname, otherwise=None):
        if fname in self.commits:
            fpath = path.join(self.folder, self.commits[fname])
        else:
            fpath = path.join(self.folder, fname)

        if path.isfile(fpath):
            print(f"Loading {fpath}")
            with open(fpath, "r") as f:
                data = TimeEntry(json.load(f))
            ret = True
        else:
            data = otherwise
            ret = False

        self.loaded[cat] = data
        self.loadedpaths[cat] = fname
        return ret

    def merge_into(self, path, content, cat):
        if cat not in self.loadedpaths:
            existed = self.load(cat, path, otherwise = content)
        elif self.loadedpaths[cat] != path:
            # Save and load new file
            self.save(cat)
            existed = self.load(cat, path, otherwise = content)
        else:
            existed = True

        if existed:
            self.loaded[cat].merge_inplace(content)

        self.unsaved.add(cat)

    def finish(self):
        # Save unsaved files
        todo = self.unsaved.copy()
        for cat in todo:
            self.save(cat)

    def commit(self, fname):
        if fname in self.commits:
            tmppath = path.join(self.folder, self.commits[fname])
            fpath = path.join(self.folder, fname)

            print(f"Commiting {tmppath} to {fpath}")
            os.rename(tmppath, fpath)

            del self.commits[fname]

class Index:
    def __init__(self, raw=set(), single={}):
        # Contains a list of raw files
        # The existence of a entry here means that that file has been accounted for
        self.raw = raw

        # Map of files which have been converted from raw files. Contains a set of raw files in it.
        self.single = single

    # Returns either a new Index instance
    def load_from_file(path):
        data = {}
        with open(path, "r") as f:
            data = json.load(f)

        single = data["single"]
        keys = single.keys()
        for key in keys:
            single[key] = set(single[key])

        return Index(raw=set(data["raw"]), single=data["single"])

    # Returns a Index, of file was not found a empty one is returned
    def load_folder(folder):
        index_path = path.join(folder, "index.json")

        if path.isfile(index_path):
            return Index.load_from_file(index_path)
        else:
            return Index()

    def save_folder(self, folder, commit=None):
        with open(path.join(folder, "index.json"), "w") as f:
            json.dump(self.as_dict(), f)

        if commit:
            for f in self.single:
                commit(f)

    def register_raw_file(self, file):
        self.raw.add(file)

    def register_single_file(self, single, raw):
        if single in self.single:
            self.single[single].add(raw)
        else:
            self.single[single] = { raw }

    def single_has_raw(self, single, raw):
        return single in self.single and raw in self.single[single]

    def as_dict(self):
        single = {}
        for key in self.single.keys():
            single[key] = list(self.single[key])

        return {
                "raw": list(self.raw),
                "single": single
                }

# Add the entry to the folders corrosponding day and all files
def add_entry(sfls, index, entry, rawname):
    def add_to_file(name, cat):
        if index.single_has_raw(name, rawname):
            print(f"{rawname} already in {name}")
            return
        sfls.merge_into(name, entry, cat)

        index.register_single_file(name, rawname)

    date = entry.start

    add_to_file(date.strftime("day%Y-%m-%d.json"), "day")
    add_to_file("all.json", "all")

def folder_cmd_f(args):
    infolder = args.infolder
    outfolder = args.outfolder

    # Load index
    index = Index.load_folder(outfolder)

    sfls = SingleFileLoadSaver(outfolder)

    files = os.listdir(infolder)
    files.sort()

    def finish():
        print(f"Saving files")
        sfls.finish()

        print(f"Saving index")
        index.save_folder(outfolder, commit=sfls.commit)

    def int_handler(signal, frame):
        print("Received interrupt, saving finished files")
        finish()
        exit(0)

    signal.signal(signal.SIGINT, int_handler)

    for file in files:
        if not raw_regex.match(file):
            continue

        # Check if we already parsed this one
        if file in index.raw:
            print(f"Skipping {file}: already in index")
            continue

        print(f"Parsing {file}")
        filesummed = TimeEntry.merge_list( 
                file_entries(path.join(infolder, file), dofiletime=True))

        print(f"Adding to summed files")
        add_entry(sfls, index, filesummed, file)

        print(f"Adding to index")
        index.register_raw_file(file)

    finish()

