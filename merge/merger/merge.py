"""
    This file is part of mirror_stats.

    mirror_stats is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    mirror_stats is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with mirror_stats.  If not, see <https://www.gnu.org/licenses/>.
"""

import json
from datetime import datetime, timedelta

datestr = "%Y-%m-%dT%H:%M:%S"

class Tree:
    def __init__(self, data):
        self.data = data

    def __merge_add_inplace(dest, src):
        dest["s"] += src["s"]
        dest["h"] += src["h"]

        if "p" in src:
            if "p" not in dest:
                dest["p"] = {}
        else:
            return

        for path in src["p"].keys():
            if path not in dest["p"].keys():
                dest["p"][path] = {"s": 0, "h": 0}
            Tree.__merge_add_inplace(dest["p"][path], src["p"][path])

    def merge_inplace(self, a):
        Tree.__merge_add_inplace(self.data, a.data)

    def to_dict(self):
        return self.data

class NoDateException(Exception):
    def __init__(self):
        self.message = "Loaded entry does not specify date, and no date has been given"

class TimeEntry:
    def __init__(self):
        pass

    def __init__(self, data, filetime=None):
        self.tree = Tree(data["r"])
        self.start = TimeEntry.__resolv_time(data, "s", filetime)
        self.end = TimeEntry.__resolv_time(data, "e", filetime)

        # Handle a stupid bug case, as 2021-02-12T00:00 should be 2021-02-13T00:00.
        # However this is hard to fix as the files do not specify date.
        # This bug is only present in files that only specify time and not date.
        if filetime != None:
            if self.start > self.end:
                self.end += timedelta(days=1)

    def __resolv_time(data, key_prefix, filetime=None):
        timestamp = ""

        # Full timestamps have the f prefix
        newkey = key_prefix + "f"
        if newkey not in data:
            if filetime is None:
                raise NoDateException()
            timestamp = f"{filetime}T{data[key_prefix]}"
        else:
            timestamp = data[newkey]

        return datetime.strptime(timestamp, datestr)

    def merge_inplace(self, a):
        self.start = min(self.start, a.start)
        self.end = max(self.end, a.end)

        self.tree.merge_inplace(a.tree)

    def to_dict(self):
        return {
                "r": self.tree.to_dict(),
                "sf": self.start.strftime(datestr),
                "ef": self.end.strftime(datestr)
                }

    def merge_list(entries):
        summed = entries[0]
        for entry in entries[1:]:
            summed.merge_inplace( entry )

        return summed

