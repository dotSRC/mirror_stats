"""
    This file is part of mirror_stats.

    mirror_stats is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    mirror_stats is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with mirror_stats.  If not, see <https://www.gnu.org/licenses/>.
"""

from .file_mode import file_cmd_f
from .folder_mode import folder_cmd_f

import argparse

def default_cmd(parser):
    def f(args):
        parser.print_help()
    return f

parser = argparse.ArgumentParser()
parser.set_defaults(func=default_cmd(parser))
subparsers = parser.add_subparsers(help="Mode")
file_cmd = subparsers.add_parser("file", help="Merge a single file")
file_cmd.add_argument("infile", help="Load from given file")
file_cmd.add_argument("--outfile", "-o", help="Save merge to file")
file_cmd.set_defaults(func=file_cmd_f)

folder_cmd = subparsers.add_parser("folder", help="Convert raw files in folders")
folder_cmd.add_argument("infolder", help="Which folder to work in")
folder_cmd.add_argument("outfolder", help="Which folder to work in")
folder_cmd.set_defaults(func=folder_cmd_f)

args = parser.parse_args()

args.func(args)
